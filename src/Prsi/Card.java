package Prsi;

public class Card {
    enum Color
    {
        Srdce, Piky, Krize, Kary;

        private static final Color[] colors =Color.values();
        public static Color getColor(int i)
        {
            return Color.colors[i];
        }

    }



    enum Value
    {
       Dva, Tri, Ctyri, Pet, Sest, Sedm, Osm, Devet, Deset, Janek, Dama, Kral, Eso;

        private static final Value[] values = Value.values();
        public static Value getValue(int i)
        {
            return Value.values[i];
        }

    }
    private final Color color;
    private final Value value;

    public Card(Color color, Value value){
        this.color = color;
        this.value = value ;
    }

    public Color getColor() {
        return this.color;
    }

    public Value getValue(){
        return this.value;
    }

    public String toString(){
        return color + "_" + value ;
    }

}
