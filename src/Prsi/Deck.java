package Prsi;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Random;

public class Deck {

    private Card[] cards;
    private int cardsInDeck;

    public Deck() {
        cards = new Card[52];
    }

    public void reset() {
        Card.Color[] colors = Card.Color.values();// Poziva hodnoty z enum Colors
        cardsInDeck = 0;
        for (int i = 0; i < colors.length - 1; i++) {
            Card.Color color = colors[i];


            for (int j = 0; j <= 13; j++) {
                cards[cardsInDeck++] = new Card(color, Card.Value.getValue(j));
            }
          Card.Value[] values = new Card.Value[]{};
                for(Card.Value value : values)
                    cards[cardsInDeck++] = new Card(color, value);
          }
        }
    public void replaceDeckWith(ArrayList<Card> cards) { // Nahrazuje balicek  arraylistem karet
        this.cards = cards.toArray(new Card[cards.size()]);
        this.cardsInDeck = this.cards.length;
    }
        public boolean isEmpty() {  //kontrola jestli je balíček prázdmy
            return cardsInDeck == 0;
        }
        public void shuffle() { //pro míchání
        int n = cards.length;
        Random random = new Random();

        for (int i =  0; i < cards.length; i++) {

            int randomValue = i + random.nextInt(n - 1);
            Card randomCard = cards[randomValue];
            cards [randomValue] = cards[i];
            cards[i] = randomCard;
        }
        }
    public Card drawCard() throws IllegalArgumentException {
        if (isEmpty()) {
            throw new IllegalArgumentException("Nemuzes si vzit kartu, protoze zadne nejsou v balicku");
        }
        return cards[--cardsInDeck];
    }

    public ImageIcon drawCardImage()throws IllegalArgumentException {
      if(isEmpty()) {
          throw new IllegalArgumentException("Nemuzes si vzit kartu, protoze zadne nejsou v balicku");

      }
      return new ImageIcon(cards[--cardsInDeck].toString() + ".png");
    }

    public Card[] drawCard(int n) {
      if (n < 0) {
          throw new IllegalArgumentException("musite vzit pozitivni pocet karet, snazite se vzit" + n + " karet");
      }
      if (n> cardsInDeck) {
          throw new IllegalArgumentException("nelze vzit"+ n + "karet, protoze v balicku je jenom"+ cardsInDeck + "karet");
      }
        Card[] ret = new Card[n];
      for(int i =0; i < n; i++) {
          ret[i] = cards[--cardsInDeck];
      }
      return ret;
    }

    }







