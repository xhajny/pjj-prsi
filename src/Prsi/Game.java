package Prsi;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Game {
    private int currentPlayer;
    private String[] playerIds;

    private Deck deck;
    private ArrayList<ArrayList<Card>> playerHand;
    private ArrayList<Card> stockpile;

    private Card.Color validColor;
    private Card.Value validValue;

    boolean gameDirection;

    public Game(String[] pids) {
        deck = new Deck();
        deck.shuffle();
        stockpile = new ArrayList<Card>();

        playerIds = pids;
        currentPlayer = 0;
        gameDirection = true;

        playerHand = new ArrayList<ArrayList<Card>>();
        for (int i = 0; i < pids.length; i++) {
            ArrayList<Card> hand = new ArrayList<Card>(Arrays.asList(deck.drawCard(7)));
            playerHand.add(hand);
        }
    }

    public void start(Game game) {
        Card card = deck.drawCard();
        validColor = card.getColor();
        validValue = card.getValue();

        if (card.getValue() == Card.Value.Sedm) {
            start(game);
        }

        if (card.getValue() == Card.Value.Eso) {
            JLabel message = new JLabel(playerIds[currentPlayer] + "byl preskocen");
            message.setFont(new Font("Arial", Font.BOLD, 48));
            JOptionPane.showMessageDialog(null, message);

            if (gameDirection == false) {
                currentPlayer = (currentPlayer + 1) % playerIds.length;
            } else if (gameDirection == true) {
                currentPlayer = (currentPlayer - 1) % playerIds.length;
                if (currentPlayer == -1) {
                    currentPlayer = playerIds.length - 1;
                }
            }
        }


        stockpile.add(card);
    }

    public Card getTopCard() {
        return new Card(validColor, validValue);
    }

    public ImageIcon getTopCardImage() {
        return new ImageIcon(validColor + "-" + validValue + ".png");
    }

    public boolean isGameOver() {
        for (String player : this.playerIds) {
            if (hasEmptyHand(player)) {
                return true;
            }
        }
        return false;
    }

    public String getCurrentPlayer() {
        return this.playerIds[this.currentPlayer];
    }

    public String getPreviousPlayer(int i) {
        int index = this.currentPlayer - i;
        if (index == -1) {
            index = playerIds.length - 1;
        }
        return this.playerIds[index];
    }

    public String[] getPlayers() {
        return playerIds;
    }

    public ArrayList<Card> getPlayerHand(String pid) {
        int index = Arrays.asList(playerIds).indexOf(pid);
        return playerHand.get(index);
    }

    public int getPlayeHandSize(String pid) {
        return getPlayerHand(pid).size();
    }

    public Card getPlayerCard(String pid, int choice) {
        ArrayList<Card> hand = getPlayerHand(pid);
        return hand.get(choice);
    }

    public boolean hasEmptyHand(String pid) {
        return getPlayerHand(pid).isEmpty();
    }

    public boolean validCardPlay(Card card) {
        return card.getColor() == validColor || card.getValue() == validValue;
    }

    public void checkPlayerTurn(String pid) throws InvalidPlayerTurnException {
        if (this.playerIds[this.currentPlayer] != pid) {
            throw new InvalidPlayerTurnException("Tohle neni kolo hrace " + pid , pid);

        }

    }

    public void submitDraws(String pid) throws InvalidPlayerTurnException {
        checkPlayerTurn(pid);
        if (deck.isEmpty()) {
            deck.replaceDeckWith(stockpile);
            deck.shuffle();
        }

        getPlayerHand(pid).add(deck.drawCard());
        if (gameDirection == false) {
            currentPlayer = (currentPlayer + 1) % playerIds.length;
        } else if (gameDirection == true) {
            currentPlayer = (currentPlayer - 1) % playerIds.length;
            if (currentPlayer == -1) {
                currentPlayer = playerIds.length - 1;
            }
        }

    }

    public void setCardColor(Card.Color color) {
        validColor = color;
    }

    public void submitPlayerCard(String pid, Card card, Card.Color declaredColor)
            throws InvalidValueSubmissionException, InvalidColorSubmissionException,InvalidPlayerTurnException{

            checkPlayerTurn(pid);

            ArrayList<Card> pHand = getPlayerHand(pid);

            if (!validCardPlay(card)) {
                if (card.getColor() == validColor) {
                    JLabel message = new JLabel("neplatny tah, ocekavana barva je : " + validColor + " ale polozil si :" + card.getColor());
                    message.setFont(new Font("Arial", Font.BOLD, 48));
                    JOptionPane.showMessageDialog(null, message);
                    throw new InvalidColorSubmissionException("neplatny tah, ocekavana barva je : " + validColor + " ale polozil si :" + card.getColor() , card.getColor(), validColor);
                } else if (card.getValue() != validValue) {
                    JLabel message2 = new JLabel("neplatny tah, ocekavana barva je : " + validValue + " ale polozil si :" + card.getValue());
                    message2.setFont(new Font("Arial", Font.BOLD, 48));
                    JOptionPane.showMessageDialog(null, message2);

                    throw new InvalidValueSubmissionException("neplatny tah, ocekavana barva je : " + validColor + " ale polozil si :" + card.getValue() , card.getValue(), validValue);
                }
            }

            pHand.remove(card);

            if (hasEmptyHand(this.playerIds[currentPlayer])) {
                JLabel message2 = new JLabel(this.playerIds[currentPlayer] + " vyhrál hru! Diky za hrani me hry <3 !");
                message2.setFont(new Font("Arial", Font.BOLD, 48));
                JOptionPane.showMessageDialog(null, message2);
                System.exit(0);
            }

            validColor = card.getColor();
            validValue = card.getValue();
            stockpile.add(card);

            if (gameDirection == false) {
                currentPlayer = (currentPlayer + 1) % playerIds.length;
            }
            else if (gameDirection == true) {
                currentPlayer = (currentPlayer - 1) % playerIds.length;
                if (currentPlayer == -1) {
                    currentPlayer = playerIds.length - 1;
                }

            }

            if (card.getValue() == Card.Value.Dama) {
                validColor = declaredColor;
            }



            if (card.getValue() == Card.Value.Sedm) {
                pid = playerIds[currentPlayer];
                getPlayerHand(pid).add(deck.drawCard());
                getPlayerHand(pid).add(deck.drawCard());
                JLabel message = new JLabel(pid + "si vezme 2 karty");
            }

            if (card.getValue() == Card.Value.Kral || card.getColor() == Card.Color.Piky) {
                pid = playerIds[currentPlayer];
                getPlayerHand(pid).add(deck.drawCard());
                getPlayerHand(pid).add(deck.drawCard());
                getPlayerHand(pid).add(deck.drawCard());
                getPlayerHand(pid).add(deck.drawCard());
                JLabel message = new JLabel(pid + "si vezme 4 karty");
            }

            if (card.getValue () == Card.Value.Eso) {
                JLabel message = new JLabel(playerIds[currentPlayer] + " stoji !");
                message.setFont(new Font("Arial", Font.BOLD, 48));
                JOptionPane.showMessageDialog(null, message);
                if (gameDirection == false) {
                    currentPlayer = (currentPlayer + 1) % playerIds.length;
            }
                else if(gameDirection == true) {
                    currentPlayer = (currentPlayer - 1 ) % playerIds.length;
                    if (currentPlayer == -1) {
                        currentPlayer = playerIds.length - 1;
                    }
                }

            }





    }

}

class InvalidPlayerTurnException extends Exception {
    String playerId;

    public InvalidPlayerTurnException(String message, String pid) {
        super(message);
        playerId = pid;
    }

    public String getPid() {
        return playerId;
    }
}

class InvalidColorSubmissionException extends Exception {
    private Card.Color expected;
    private Card.Color actual;

    public InvalidColorSubmissionException(String message, Card.Color actual, Card.Color expected) {
        this.actual = actual;
        this.expected = expected;
    }
}

class InvalidValueSubmissionException extends Exception {
    private Card.Value expected;
    private Card.Value actual;

    public InvalidValueSubmissionException(String message, Card.Value actual, Card.Value expected) {
        this.actual = actual;
        this.expected = expected;
    }
}